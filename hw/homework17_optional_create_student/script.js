// Создать объект "студент" и проанализировать его табель. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:
// - Создать пустой объект `student`, с полями `name` и `lastName`.
// - Спросить у пользователя имя и фамилию студента, полученные значения записать в соответствующие поля объекта.
// - В цикле спрашивать у пользователя название предмета и оценку по нему. Если пользователь нажмет Cancel при n-вопросе о названии предмета, закончить цикл. Записать оценки по всем предметам в свойство студента `tabel`.
// - Посчитать количество плохих (меньше 4) оценок по предметам. Если таких нет, вывести сообщение `Студент переведен на следующий курс`.
// - Посчитать средний балл по предметам. Если он больше 7 - вывести сообщение `Студенту назначена стипендия`.

let student = {};

let userName = prompt("Enter your name");
let userLastName = prompt("Enter your last name");

student.name = userName;
student.lastName = userLastName;
student.tabel = {};

let lesson = prompt("Enter your lesson");
let rating = +prompt("Enter your rating");
student[lesson] = rating;

while (true) {
  lesson = prompt("Enter your lesson"); 
  if(!lesson) {
    break;
  }
  rating = +prompt("Enter your rating");
  student.tabel[lesson] = rating;
}

let countRatings = 0;
let countBadRating = 0;
let averageRating = 0;
let summRating = 0;

for (let key in student["tabel"]) {
    countRatings++;
    summRating += student["tabel"][key]
  if (student["tabel"][key] < 4) {
    countBadRating++;
  }
}

function getAverageRating (sumRat, countRat) {
   return averageRating = sumRat / countRat
}

if(countBadRating === 0) {
    if(getAverageRating(summRating, countRatings) > 7) {
        alert(`Студент ${student.name} ${userLastName} переведен на следующий курс. Студенту ${student.name} назначена стипендия.`)
    }else{
        alert(`Студент ${student.name} ${userLastName} переведен на следующий курс.`)
    }
}else{
    alert(`Количество плохих оценок по предметам ${countBadRating}`);
}