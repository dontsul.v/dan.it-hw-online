
function createTable() {
  const table = document.createElement("table");

  document.body.prepend(table);

  document.body.addEventListener("click", changeColor);

  function changeColor(event) {
    if (event.target.nodeName === "TD") {
      event.target.classList.toggle("selected");
    } else if (event.target.nodeName === "BODY") {
      event.target.firstElementChild.classList.toggle("inverted");
    }
  }

  for (let i = 0; i < 30; i++) {
    let tr = document.createElement("tr");
    table.append(tr);

    for (let k = 0; k < 30; k++) {
      let td = document.createElement("td");
      td.style.cssText = `
      width: 25px;
      height: 25px;
      border: 1px solid yellow;
      `;
      tr.append(td);
    }
  }
}
createTable();
