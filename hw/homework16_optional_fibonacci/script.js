
let number = +prompt('Enter your number');

function fibonacci(number) {
    return (number <= 1) ? number : fibonacci(number - 1) + fibonacci(number - 2)
}


function fibonacci2(number) {
    return (number >= -1) ? number : fibonacci(number + 1) + fibonacci(number + 2)
}

if(number > 0) {
    console.log(fibonacci(number));
}else {
    console.log(fibonacci2(number));
}