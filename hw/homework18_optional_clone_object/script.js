// Реалізувати функцію повного клонування об'єкта. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:
// - Написати функцію для  повного рекурсивного клонування об'єкта (без єдиної передачі за посиланням, внутрішня вкладеність властивостей об'єкта може бути досить великою).
// - Функція повинна успішно копіювати властивості у вигляді об'єктів та масивів на будь-якому рівні вкладеності.
// - У коді не можна використовувати вбудовані механізми клонування, такі як функція Object.assign() або спред-оператор.

let auto = {
  title: "vw",
  year: 2030,
  fuel: "diesel",
  country: [
    "Ukraine",
    "USA",
    "Spain",
    {
      one: 1,
      two: 2,
      three: [1, 2, 3],
    },
  ],
  engine: {
    "engine volume": 5,
    isTurbo: true,
  },
};


function copyObj(obj) {
    let newObj = {};
  for (let key in obj) {

    if (typeof obj[key] === "object" && Array.isArray(obj[key]) === false) {
      newObj[key] = copyObj(obj[key]);
    }else if(typeof obj[key] !== "object") {
      newObj[key] = obj[key];
    }
//     else if(typeof obj[key] === "object" && Array.isArray(obj[key])) {
//       for(let item of obj[key]) {
//           if(typeof item !== 'object') {
//             obj[key].push(item)
//           }
//       }
// }
  }
  return newObj;
}
copyObj(auto);
let copy = copyObj(auto);





console.log("-----------------");

console.log(copy);
console.log(auto);
copy.engine.isTurbo = false;
console.log(copy);
console.log(auto);
