// ## Задание

// Реализовать переключение вкладок (табы) на чистом Javascript.

// #### Технические требования:
// - В папке `tabs` лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.
// - Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
// - Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.

function setTab() {
    let listTabsTitle = document.querySelectorAll(".tabs-title");
    let listTabsContent = document.querySelectorAll('.content');
    let attribute;

  listTabsTitle.forEach((item) => {
    item.addEventListener("click", addClick);
  });

  function addClick() {
    listTabsTitle.forEach((item) => {
      item.classList.remove("active");
    });
    this.classList.add('active');
    attribute = this.dataset.name;
    containContent(attribute);
  }

  function containContent(attr) {
    listTabsContent.forEach((item) => {
    item.classList.contains(attr) ? item.classList.add('active-content') : item.classList.remove('active-content')
  })}
}
setTab();
