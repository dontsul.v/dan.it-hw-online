"use strict";

function createCircle() {
  const btn = document.querySelector(".btn");
  btn.addEventListener("click", createInput);

  function createInput() {
    const div = document.createElement("div");
    div.style.cssText = `
        display: flex;
        justify-content: center;`;
    document.body.append(div);

    const input = document.createElement("input");
    input.style.marginRight = "15px";
    input.type = "number";
    input.placeholder = "Введите диаметр";

    const btnDraw = document.createElement("button");
    btnDraw.textContent = "Нарисовать";

    div.append(input);
    div.append(btnDraw);

    btnDraw.addEventListener("click", createAllCircle);

    function createAllCircle() {
      let valueInput = input.value;
      let divContainer = document.createElement("div");
      divContainer.style.cssText = `
        display: flex;
        flex-wrap: wrap;`;
      document.body.append(divContainer);

      function randomNumber() {
        return Math.floor(0 + Math.random() * (255 + 1 - 0));
      }

      for (let i = 0; i < 100; i++) {
        let divCircle = document.createElement("div");
        divCircle.classList.add("div-circle");
        divCircle.style.cssText = `
                width: ${+valueInput}px;
                height: ${+valueInput}px;
                border-radius: 50%;
                background-color: rgb(${randomNumber()},${randomNumber()},${randomNumber()});
                margin: 10px;`;
        divContainer.append(divCircle);
      }
      divContainer.addEventListener("click", removeCircle);

      function removeCircle() {
        if (event.target.classList.contains("div-circle")) {
          event.target.remove();
        }
      }
    }
  }
}
createCircle();
