// 1. Напишите как вы понимаете рекурсию. Для чего она используется на практике?
// Рекурсия - это когда функция вызывает сама себя. Её, как правило, используют когда у элемента есть большая вложенность и их необходимо перебрать.

// ## Задание

// Реализовать функцию подсчета факториала числа. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).


let userNumber = prompt('Enter your number');
while (Number.isNaN(+userNumber) || userNumber == "" || userNumber == null) {
    userNumber = +prompt('Enter your number', userNumber);
}

function factorial(numb) {
    if(numb === 1) {
      return 1;
    }else{
      return numb * factorial(numb - 1)
    }
  }
alert(factorial(userNumber))
