// ## Теоретический вопрос

// 1. Опишите каким образом можно создать новый HTML тег на странице.
// с помощью метода createElement('elem'). например let newTag = document.createElement('div');
// 2. Опишите что означает первый параметр функции insertAdjacentHTML и опишите возможные варианты этого параметра. 
// первый параметр функции insertAdjacentHTML означает куда будет помещен элемент со 2 параметра. существуют: beforebegin, afterbegin, beforeend, afterend
// 3. Как можно удалить элемент со страницы?
//с помощь метода remove(). напимер elem.remove();

// ## Задание

// Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонних библиотек (типа Jquery).

// #### Технические требования:

// - Создать функцию, которая будет принимать на вход массив и опциональный второй аргумент parent - DOM-элемент, к которому будет прикреплен список (по дефолту должен быть document.body).
// - Каждый из элементов массива вывести на страницу в виде пункта списка;

// Примеры массивов, которые можно выводить на экран:
// ```javascript
// 
// ```
// ```javascript
// ["1", "2", "3", "sea", "user", 23];
// ```

// - Можно взять любой другой массив.

// #### Необязательные задания продвинутой сложности:

// 1. Добавьте обработку вложенных массивов. Если внутри массива одним из элементов будет еще один массив, выводить его как вложенный список.
//    Пример такого массива:

//    ```javascript
//    
//    ```

//    > Подсказка: используйте map для обхода массива и рекурсию, чтоб обработать вложенные массивы.

// 2. Очистить страницу через 3 секунды. Показывать таймер обратного отсчета (только секунды) перед очищением страницы.

let array = ["Kharkiv", "Kiev", ["Borispol",['New York', 'Sidney'], "Irpin"], "Odessa", "Lviv", "Dnieper"];
let body = document.querySelector('body');
let newArr = [];

function showItem(arr, parent) {
    
    function deepCloneArr(arr) {
        for(let item of arr) {
            if(typeof item !== 'object') {
                newArr.push(item)
            }else{
                item.push(deepCloneArr(item))
            }
        }
        return newArr
    }
    (deepCloneArr(arr));

    let ul = document.createElement('ul');
    parent.append(ul);
    
    newArr.map(item => {
        let li = document.createElement('li');
        li.textContent = item;
        ul.append(li);
    })

    let div = document.createElement('div');
    div.classList.add('timer');
    div.innerHTML = '3';
    body.prepend(div)

    let timerId = setInterval(() => {
        if(div.innerHTML > 0) {
            div.innerHTML = div.innerHTML - 1;
        }else{
            div.innerHTML = 'it\'s over'; 
            clearInterval(timerId);
        }
    }, 1000)
    
    setTimeout(() => {
        ul.remove()
    }, 3000)
}
showItem(array, body);


