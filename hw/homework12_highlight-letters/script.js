// 1. Почему для работы с input не рекомендуется использовать события клавиатуры?
// потому что, значение в input можно copy/past, вставить с помощью голосовых инструментов


function backlight() {
    const btns = document.querySelectorAll(".btn");

    document.addEventListener("keydown", function (event) {
        console.log(event.code);
        console.log(event.key);
      let pressKey = event.key.toLocaleUpperCase();
      btns.forEach((item) => {
        let itemUt = item.textContent.toLocaleUpperCase();
        if (pressKey === itemUt) {
          item.style.backgroundColor = "blue";
        }else{
            item.style.backgroundColor = "";
        }
      });
    });
}

backlight();