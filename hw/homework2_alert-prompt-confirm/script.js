// ## Теоретические вопросы

// 1. Какие существуют типы данных в Javascript?
//string, number, boolean, null, undefind, bigInt, symbol, object

// 2. В чем разница между == и === ?
// '===' - строгое равенство, проверяет тип данных значений, если тпи данных одинаковый, сравнивает значения, возвращает true or false.
// '=='- нестрогое равенство приводят значения к одному типу и тогда сравнивает значения.

// 3. Что такое оператор?
// Это встроенная функция JSб которая выполянте выполняет определённые действия с операндами.

let userName = prompt("Enter your name");
while (!userName) {
  userName = prompt("Enter your name", userName);
}

let userAge = prompt("Enter your age");
while (Number.isNaN(+userAge) || userAge == "" || userAge == null) {
  userAge = +prompt("Enter your age", userAge);
}

if (userAge < 18) {
  alert(`You are not allowed to visit this website`);
} else if (userAge >= 18 && userAge <= 22) {
  let answer = confirm(`Are you sure you want to continue?`);
  if (answer) {
    alert(`Welcome, ${userName}`);
  } else {
    alert(`You are not allowed to visit this website`);
  }
} else {
  alert(`Welcome, ${userName}.`);
}
