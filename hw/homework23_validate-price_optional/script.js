'use strict'

document.addEventListener("DOMContentLoaded", createElem);

function createElem() {
  const div = document.createElement("div");
  div.classList.add("hide");

  const span = document.createElement("span");
  span.style.marginRight = "20px";

  const btn = document.createElement("button");
  btn.textContent = "X";

  div.append(span);
  div.append(btn);
  div.style.position = "absolute";

  document.body.prepend(div);

  let check = true;

  const divWrap = document.createElement("div");
  divWrap.style.position = "relative";
  divWrap.style.top = "40px";

  let invalidPrice = document.createElement("span");
  invalidPrice.textContent = "Please enter correct price";

  const input = document.createElement("input");
  input.placeholder = "Price";
  input.type = "number";
  input.style.display = "block";
  divWrap.append(input);

  input.addEventListener("focus", () => {
    input.style.outline = "2px solid green";
  });

  input.addEventListener("blur", () => {
    let value = input.value;

    if (value < 0) {
      check = false;
      input.style.outline = "2px solid red";
      div.classList.add("hide");

      divWrap.append(invalidPrice);
    } else {
      if (check === false) {
        invalidPrice.remove();
      }

      input.style.outline = "";
      input.style.color = "green";

      div.classList.add("active");
      div.classList.remove("hide");
      span.textContent = `Текущая цена: ${value}`;

      btn.addEventListener("click", () => {
        div.classList.add("hide");

        input.value = "";
        input.style.color = "";
      });
    }
  });

  document.body.append(divWrap);
}
