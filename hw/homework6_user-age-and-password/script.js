// ## Теоретический вопрос

// 1. Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
//экранирование - специальный символ,который используется в регулярных выражениях и в строках.
// Мы делаем экранирование для того, что-бы интерпретатор распознавал спец-символы как текст, а не часть кода. 

// 2. Какие способы объявления функций вы знаете?
// function declaration, function expression, arrow function, new Function, Generator function
// 3. Что такое hoisting, как он работает для переменных и функций?
// hoisting - механизм в JavaScript, который переменные и function declaration поднимает вверх своей области видимости. То есть когда интерпретатор начинает читать код, он впервую очередь запомнит все переменные и function declaration, а потом идет по порядку. 
// ## Задание

// Дополнить функцию `createNewUser()` методами подсчета возраста пользователя и его паролем. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:

// - Возьмите выполненное домашнее задание номер 4 (созданная вами функция `createNewUser()`) и дополните ее следующим функционалом:
//   1.  При вызове функция должна спросить у вызывающего дату рождения (текст в формате `dd.mm.yyyy`) и сохранить ее в поле `birthday`.
//   2.  Создать метод `getAge()` который будет возвращать сколько пользователю лет.
//   3.  Создать метод `getPassword()`, который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, `Ivan Kravchenko 13.03.1992 → Ikravchenko1992`).
// - Вывести в консоль результат работы функции `createNewUser()`, а также функций `getAge()` и `getPassword()` созданного объекта.



function createNewUser() {
  let userName = prompt('Enter your name');
  let userSurname = prompt('Enter your surname');
  let userBirthday = prompt('Введите дату рождения в формате dd.mm.yyyy', 'dd.mm.yyyy');

  

  let currentDate = new Date();
  const [day, month, year] = userBirthday.split('.');
  const dateBirthReverse = `${year}.${month}.${day}`;
  
  const dateBirth = new Date(dateBirthReverse);

  let newUser = {};

  newUser.firstName = userName;
  newUser.lastName = userSurname;
  newUser.birthday = userBirthday;

  newUser.getLogin = function () {
    return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
  };
  let result = newUser.getLogin();

  newUser.getAge = function() {
    
    return currentDate.getFullYear() - dateBirth.getFullYear();
  }
  let resultAge = newUser.getAge();

  newUser.getPassword = function() {
    return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + dateBirth.getFullYear()
  }
  let password = newUser.getPassword();

  console.log(password);
  console.log(resultAge);
  console.log(result);
  console.log(newUser);

}

createNewUser();


