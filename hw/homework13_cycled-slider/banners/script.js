// 1. Опишите своими словами разницу между функциями `setTimeout()` и `setInterval()`.
// setTimeout() выполняется 1 раз через указанное время, setInterval() выполняется интервально через указанное время, пока не будет остановлено. 
// 2. Что произойдет, если в функцию `setTimeout()` передать нулевую задержку? Сработает ли она мгновенно, и почему?
// c 0 задержкой планирует вызов функции настолько быстро, насколько это возможно. сработает только тогда, когда закончиться выполнять текущий код.  
// 3. Почему важно не забывать вызывать функцию `clearInterval()`, когда ранее созданный цикл запуска вам уже не нужен?
// потому что функция продолжит вызываться.

function showImage() {
  const imagesWrapper = document.querySelector(".images-wrapper");

  const imageList = document.querySelectorAll(".image-to-show");

  const containerBtn = document.createElement("div");
  containerBtn.style.cssText = `
  display:flex;
  justify-content: center;
  margin-top: 40px;
  `;

  const btnStop = document.createElement("button");
  btnStop.style.marginRight = "20px";
  btnStop.textContent = "Прекратить";

  const btnStart = document.createElement("button");
  btnStart.textContent = "Возобновить показ";

  containerBtn.append(btnStop);
  containerBtn.append(btnStart);

  imagesWrapper.after(containerBtn);

  document.addEventListener("DOMContentLoaded", setSlider);

  let indexItemFirst = 1;
  function setSlider() {
    let timeout = 3000;
    let indexItem = indexItemFirst;

    let interval = setInterval(() => {
      if (indexItem > 0) {
        for (let item of imageList) {
          item.classList.remove("is-active");
        }
      }

      if (indexItem > imageList.length - 1) indexItem = 0;

      imageList[indexItem].classList.add("is-active");
      indexItem++;

      btnStop.addEventListener("click", () => {
        indexItemFirst = indexItem;
        clearInterval(interval);
      });
      btnStart.addEventListener("click", () => {
        setSlider();
      });
    }, timeout);
  }
}

showImage();
