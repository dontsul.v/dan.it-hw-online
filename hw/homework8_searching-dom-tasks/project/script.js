//## Теоретические вопросы
//1. Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
//DOM- модель документа, которая представляет всё содержимое ввиде объектов.
//2. Какая разница между свойствами HTML-элементов innerHTML и innerText?
// innerHtml выводит содержимое с учетом html тегов в которые оно записано, innerText проигнорирует теги, и будет выводить только содержимое этих тегов, даже если теги будут вложены  будут вложены один в другой.
//3. Как можно обратится к элементу страницы с помощью JS? Какой способ предпочтительнее?
//querySelector, querySelectorAll, getElementById, getElementsByName, getElementsByTagName, getElementsByClassName
//querySelector, querySelectorAll - универсальные методы, лучше пользоваться ими.


//1)
let allP = document.querySelectorAll('p');
for(let item of allP) {
    item.style.backgroundColor = '#ff0000';
}
//2)
let optionsList = document.querySelector('#optionsList');
console.log(optionsList);
console.log('родительский элемент:',optionsList.parentElement);
//console.log('дочерние ноды:', optionsList.childNodes );
for(let item of optionsList.childNodes) {
    console.log(item.nodeType, item);
    
}

//3)
document.querySelector('#testParagraph').innerHTML = `<p>This is a paragraph</p>`;

//4)
let liItem = document.querySelectorAll('.main-header > div > ul > li');
 
for(let item of liItem) {
    item.classList.add('nav-item');
    console.log(item);
}

//5)
let listItem = document.querySelectorAll('.section-title');
for(let item of listItem) {
    item.classList.remove('section-title');
}